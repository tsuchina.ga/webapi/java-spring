# java-spring


## 参考にしたサイト

* [Dockerで始めるSpring Boot - Qiita](https://qiita.com/ken0909/items/a3f8594ce677bbc7c4c2)
* [Spring Boot + PostgreSQLの設定方法 - Qiita](https://qiita.com/ry0takaha4/items/55cbb88fd0d1b8ae4721)
* [コントローラからサービスを呼び出す。MVC揃える - Qiita](https://qiita.com/yukihigasi/items/befb7425731bd29aeb7a)
* [Spring Boot 使い方メモ - Qiita](https://qiita.com/opengl-8080/items/05d9490d6f0544e2351a)
* [Java: Spring Boot で REST なアプリを作ってみる - rakugakibox.net](http://blog.rakugakibox.net/entry/2014/11/23/java_spring_boot_rest)
* [Spring Bootでjson文字列を受信してクラスに割り当てる - m_shige1979のささやかな抵抗と欲望の日々](http://m-shige1979.hatenablog.com/entry/2016/12/26/080000)
* [Spring Data JPA でのクエリー実装方法まとめ - Qiita](https://qiita.com/tag1216/items/55742fdb442e5617f727)


## 環境構築

0. プロジェクトの作成

    0. [Spring Initializr](https://start.spring.io/)にアクセス
    0. プロジェクトファイルのダウンロード
        0. Generate a **Gradle Project** with **Java** and Spring Boot **2.0.0**
        0. Group: 任意
        0. Artifact: 任意
        0. Dependencies
            * **Web**
            * **JPA**
            * **Rest Repositories HAL Browser**
            * **Lombok**
            * **PostgreSQL**
        0. Generate Project
    0. 任意のディレクトリに解凍
    0. application.propertiesへの追記

        ```
        spring.datasource.driver-class-name=org.postgresql.Driver
        spring.datasource.url=jdbc:postgresql://172.17.0.2:5432/webapi
        spring.datasource.username=postgres
        spring.datasource.password=""
        ```


0. docker環境の構築

    共有するディレクトリに事前にプロジェクトファイル一式を設置しておく。

    ```
    > docker run -d -it --name java-spring -p 8080:8080 -v E:/Projects/tsuchinaga/webapi/java-spring:/root openjdk:8
    > docker exec -it java-spring bash

    $ cd /root
    $ ./gradlew compileJava
    $ ./gradlew bootRun
    ```
