package ga.tsuchina.v1.models;

import javax.persistence.*;
import lombok.*;

import java.util.Date;
import java.lang.reflect.Field;

import org.springframework.hateoas.ResourceSupport;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User extends ResourceSupport {

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @Column(name="id")
  private Integer userId;

  @Column(name="name")
  private String name;

  @Column(name="age")
  private Integer age;

  @Column(name="is_deleted")
  private Integer isDeleted;

  @Column(name="created_at", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdAt;

  @Column(name="updated_at", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
  @Temporal(TemporalType.TIMESTAMP)
  private Date updatedAt;

  public User(String name, Integer age) {
    if (name != null) this.name = name;
    if (age != null) this.age = age;
  }

  public void merge(User user) {
    for (Field field: this.getClass().getDeclaredFields()) {
      try {
        // System.out.println("this -> " + field.getName() + ": " + field.get(this));
        // System.out.println("user -> " + field.getName() + ": " + field.get(user));
        if (field.get(this) == null) field.set(this, field.get(user));
      } catch (IllegalAccessException e) {
        System.out.println(field.getName() + " is not fund");
      }
    }
  }
}
