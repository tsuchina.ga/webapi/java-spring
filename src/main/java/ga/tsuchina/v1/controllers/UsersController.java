package ga.tsuchina.v1.controllers;

import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import org.springframework.hateoas.Resources;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.validation.annotation.Validated;

import ga.tsuchina.v1.services.UsersService;
import ga.tsuchina.v1.models.User;
import ga.tsuchina.v1.forms.UserForm;

@RestController
@RequestMapping("/v1/users")
public class UsersController {

  @Autowired
  UsersService service;

  @RequestMapping(value="",method=RequestMethod.GET)
  public Resources<User> get(){
    List<User> users = service.findByIsDeleted(0);
    for (User user: users) {
      user.add(linkTo(UsersController.class).slash(user.getUserId()).withSelfRel());
    }
    return new Resources<>(users, linkTo(methodOn(UsersController.class).get()).withSelfRel());
  }

  @RequestMapping(value="",method=RequestMethod.POST)
  public HttpEntity<User> post(@RequestBody @Validated UserForm form){
    User user = service.create(new User(form.getName(), form.getAge()));
    user.add(linkTo(UsersController.class).slash(user.getUserId()).withSelfRel());
    return new ResponseEntity <> (user, HttpStatus.OK);
  }

  @RequestMapping(value="{id}",method=RequestMethod.GET)
  public HttpEntity<User> getId(@PathVariable Integer id){
    Optional<User> oUser = service.findById(id);
    User user = new User();
    if (oUser.isPresent() == true) {
      user = oUser.get();
    }
    user.add(linkTo(methodOn(UsersController.class).getId(id)).withSelfRel());
    return new ResponseEntity <> (user, HttpStatus.OK);
  }

  @RequestMapping(value="{id}",method=RequestMethod.PUT)
  public HttpEntity<User> putId(@PathVariable Integer id, @RequestBody UserForm form) {
    Optional<User> oUser = service.updateById(id, new User(form.getName(), form.getAge()));
    User user = new User();
    if (oUser.isPresent() == true) {
      user = oUser.get();
    }
    user.add(linkTo(methodOn(UsersController.class).putId(id, form)).withSelfRel());
    return new ResponseEntity <> (user, HttpStatus.OK);
  }

  @RequestMapping(value="{id}",method=RequestMethod.DELETE)
  public HttpEntity<User> deleteId(@PathVariable Integer id) {
    Optional<User> oUser = service.deleteById(id);
    User user = new User();
    if (oUser.isPresent() == true) {
      user = oUser.get();
    }
    user.add(linkTo(methodOn(UsersController.class).deleteId(id)).withSelfRel());
    return new ResponseEntity <> (user, HttpStatus.OK);
  }

}
