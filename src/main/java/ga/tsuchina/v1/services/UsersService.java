package ga.tsuchina.v1.services;

import java.util.List;
import java.util.Date;
import java.util.Optional;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Sort;

import ga.tsuchina.v1.repositories.UsersRepository;
import ga.tsuchina.v1.models.User;

@Service
@Transactional
public class UsersService {

  @Autowired
  UsersRepository repository;

  public List<User> findAll() {
    return repository.findAll(new Sort(Sort.Direction.ASC,"id"));
  }

  public User create(User user) {
    user.setIsDeleted(0);
    if (user.getCreatedAt() == null) user.setCreatedAt(new Date());
    if (user.getUpdatedAt() == null) user.setUpdatedAt(new Date());
    return repository.save(user);
  }

  public Optional<User> findById(Integer id) {
    return repository.findById(id);
  }

  public List<User> findByIsDeleted(Integer isDeleted) {
    return repository.findByIsDeleted(isDeleted);
  }

  public Optional<User> updateById(Integer id, User user) {
    try {
      User orgUser = repository.findById(id).get();
      System.out.println(orgUser);
      System.out.println(user);
      user.merge(orgUser);
      user.setIsDeleted(orgUser.getIsDeleted());
      user.setCreatedAt(orgUser.getCreatedAt());
      user.setUpdatedAt(new Date());
      return Optional.of(repository.save(user));
    } catch (NoSuchElementException e) {
      return null;
    }
  }

  public Optional<User> deleteById(Integer id) {
    try {
      User user = repository.findById(id).get();
      user.setIsDeleted(1);
      user.setUpdatedAt(new Date());
      return Optional.of(repository.save(user));
    } catch (NoSuchElementException e) {
      return null;
    }
  }

}
