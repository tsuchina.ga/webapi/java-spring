package ga.tsuchina.v1.forms;

import java.io.Serializable;

import javax.validation.constraints.*;
import lombok.Data;

@Data
public class UserForm implements Serializable {

  private static final long serialVersionUID = 1L;

  @NotNull(message = "Name is require")
  @NotEmpty(message = "Name is not empty")
  private String name;

  @Min(0)
  @Max(140)
  private Integer age;
}
